#include <bits/stdc++.h>
using namespace std;

/**
 * Best resource = https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/discuss/108870/Most-consistent-ways-of-dealing-with-the-series-of-stock-problems
 * Recurrence relation for max profit atmost K transactions =>
 * At ith day we have choice :
 * 1. Buy ith share (that means currently we should not have had any shares)
 * 2. Sell the previous share (that means we already have one share with us)
 * 3. Rest
 * dp[i][k][0] = max profit in [0....i] days with atmost k transactions with 0 shares with us at the end of ith day
 * dp[i][k][1] = max profit in [0....i] days with atmost k transactions with 1 share with us at the end of ith day
 * dp[i][k][0] = max(
 *                  dp[i - 1][k][0] //rest
 *                  dp[i - 1][k][1] + prices[i] //sell on ith day
 *              )
* dp[i][k][1] = max(
 *                  dp[i - 1][k][1] //rest
 *                  dp[i - 1][k][0] - prices[i] //buy on ith day (subtracting prices[i] since we need profit)
 *              )
 */

class Solution {
   public:
    /**
     * https://leetcode.com/problems/best-time-to-buy-and-sell-stock/
     * If you were only permitted to complete at most one transaction (i.e., buy one and sell one share of the stock), design an algorithm to find the maximum profit.
     * Problem reduces to max(arr[j] - arr[i]) s.t. j > i
     */

    int maxProfit_atmost_1_transaction(vector<int>& prices) {
        //O(N) Time | O(N) Space
        // int n = prices.size();
        // if (n == 0 || n == 1)
        //     return 0;
        // int maxOnRight[n];
        // maxOnRight[n - 1] = prices[n - 1];
        // for (int i = n - 2; i >= 0; --i) {
        //     maxOnRight[i] = max(prices[i], maxOnRight[i + 1]);
        // }

        // int max_profit = 0;
        // for (int i = 0; i < n - 1; ++i) {
        //     max_profit = max(max_profit, maxOnRight[i + 1] - prices[i]);
        // }
        // return max_profit;

        //O(N) Time | O(1) Space
        int n = prices.size();
        if (n == 0 || n == 1)
            return 0;

        int max_profit = 0;
        int maxOnRight = prices[n - 1];
        for (int i = n - 2; i >= 0; --i) {
            maxOnRight = max(prices[i], maxOnRight);
            max_profit = max(max_profit, maxOnRight - prices[i]);
        }
        return max_profit;
    }
    //Max transactions possible are actually N/2. Since transaction needs two days.
    int maxProfit_infinite_transaction(vector<int>& prices) {
        int n = prices.size();
        //For n = 0 initially
        int dp_i_0 = 0;        //at n=0, 0 stocks at hand then 0 profit since no input at all
        int dp_i_1 = INT_MIN;  //at n=0, 1 stock at hand is impossible case when there is no input, so -INF

        for (int price : prices) {
            int dp_i_0_old = dp_i_0;
            int dp_i_1_old = dp_i_1;
            dp_i_0 = max(dp_i_0_old, dp_i_1_old + price);
            dp_i_1 = max(dp_i_1_old, dp_i_0_old - price);
        }

        return dp_i_0;
    }

    //O(NK) Time | O(N*K) Space. we can do knapsack type optimization
    int max_profit_atmost_k_transactions(vector<int>& prices, int K) {
        int N = prices.size();
        int dp[N + 1][K + 1][2];
        //dp[i][k][0] = max profit which we can get in first i days atmost k transactions with 0 stocks at the end of (i-1)th day
        //dp[i][k][1] = max profit which we can get in first i days atmost k transactions with 1 stocks at the end of (i-1)th day

        for (int k = 0; k <= K; ++k) {
            dp[0][k][0] = 0;
            dp[0][k][1] = INT_MIN;  //impossible case
        }
        for (int n = 0; n <= N; ++n) {
            dp[n][0][0] = 0;
            dp[n][0][1] = INT_MIN;  //impossible case
        }

        for (int n = 1; n <= N; ++n) {
            for (int k = 1; k <= K; ++k) {
                dp[n][k][0] = max(dp[n - 1][k][1] + prices[n - 1], dp[n - 1][k][0]);
                dp[n][k][1] = max(dp[n - 1][k][1], dp[n - 1][k - 1][0] - prices[n - 1]);
            }
        }
        return dp[N][K][0];
    }

    //Space optimized. Based on knapsack optimization
    int max_profit_atmost_k_transactions(vector<int>& prices, int K) {
        int N = prices.size();
        if (N == 0 || N == 1)
            return 0;

        //Very IMP optimization. number of transactions we can make is N/2.
        //If K is very large then we will waste lot of space so when K >= N/2 that case is same as infinite transactions allowed
        if (K >= N / 2) {
            return maxProfit_infinite_transaction(prices);
        }
        //Initially we are considering values for n = 0 (no items)
        vector<int> dp_n_0(K + 1, 0);
        vector<int> dp_n_1(K + 1, INT_MIN);
        //dp[i][k][0] = max profit which we can get in first i days atmost k transactions with 0 stocks at the end of (i-1)th day
        //dp[i][k][1] = max profit which we can get in first i days atmost k transactions with 1 stocks at the end of (i-1)th day
        for (int n = 1; n <= N; ++n) {
            for (int k = K; k >= 1; --k) {
                dp_n_0[k] = max(dp_n_0[k], dp_n_1[k] + prices[n - 1]);
                dp_n_1[k] = max(dp_n_1[k], dp_n_0[k - 1] - prices[n - 1]);
            }
        }
        return dp_n_0[K];
    }

    /**
     * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-cooldown/
     * Variation of infinite transactions
     * Cannot buy on ith day if we sold on (i-1)th day
     * T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])
     * T[i][k][1] = max(T[i-1][k][1], T[i-2][k][0] - prices[i])
     */

    /**
     * https://leetcode.com/problems/best-time-to-buy-and-sell-stock-with-transaction-fee/description/
     * Another Variation of infinite transactions
     * After a transaction is done we are charged a "fee"
     * So after you sold a share subtract the fee
     * T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i] - fee)
     * T[i][k][1] = max(T[i-1][k][1], T[i-1][k][0] - prices[i])
     * ALternatively, we can also subtract the fee when we buy the share
     * T[i][k][0] = max(T[i-1][k][0], T[i-1][k][1] + prices[i])
     * T[i][k][1] = max(T[i-1][k][1], T[i-1][k][0] - prices[i] - fee)
     */
};