#include <bits/stdc++.h>
using namespace std;

//See B2B Swe youtube video for good explanation
//Based on choice spaces at each index
//Variation = Find next greater number with same set of digits
vector<int> nextPermutation(vector<int> &A) {
    int N = A.size();
    int startIndex = N - 1;  // starting index of decreasing subarray
    while (startIndex >= 1 && A[startIndex - 1] > A[startIndex]) {
        startIndex--;
    }
    //if it is 0 then entire string is in decreasing order. It is last permutation.
    //so result should be first permutation
    if (startIndex != 0) {
        // find next greater element to A[startIndex-1] in [startIndex...N-1] subarray
        int greaterIndex;
        //Can do binary search since [startIndex...N-1] is in decreasing order
        for (int i = N - 1; i >= startIndex; --i) {
            if (A[startIndex - 1] < A[i]) {
                greaterIndex = i;
                break;
            }
        }
        // swap A[sI] & A[gI]
        int temp = A[greaterIndex];
        A[greaterIndex] = A[startIndex - 1];
        A[startIndex - 1] = temp;
    }
    // reverse [startIndex...N-1] subarray
    reverse(A.begin() + startIndex, A.end());
    return A;
}
