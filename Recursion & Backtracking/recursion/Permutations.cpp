#include <bits/stdc++.h>
using namespace std;

/**
 * IMP https://www.geeksforgeeks.org/distinct-permutations-string-set-2/
 * Handling duplicates
 */

//Is it safe to swap str[i] & str[j]?
//We don't want duplicates. So return false if they are equal

/*
    Check for "ACBC"
    A will be swaped with C at index 1 and C at index 3
    CABC and CCBA and these strings will form same permutations.
    So you need to check in [i + 1....j] if there is any char with str[j]
*/
bool isSafeWrong(string str, int i, int j) {
    return str[i] != str[j] || i == j;
}

bool isSafe(string str, int i, int j) {
    for (int k = i + 1; k <= j - 1; ++k) {
        if (str[k] == str[j]) {
            //Then no use of swapping if we already swapped same char before
            return false;
        }
    }
    return str[i] != str[j] || i == j;
}
void permutations(string s, int start = 0) {
    int n = s.length();
    if (start >= n) {
        cout << s << endl;
        return;
    }

    for (int ind = start; ind < n; ++ind) {
        if (isSafe(s, start, ind)) {
            swap(s[ind], s[start]);
            permutations(s, start + 1);
            swap(s[ind], s[start]);
        }
    }
}

/**
 * Problem -> https://www.interviewbit.com/problems/permutations/
 * C++->swap() implicit function,
*/
void permutations(vector<int> A, int start, vector<vector<int> > &result) {
    if (start == A.size() - 1) {
        result.push_back(A);
        return;
    }
    for (int i = start; i < A.size(); i++) {
        swap(A[i], A[start]);
        permutations(A, start + 1, result);
        swap(A[i], A[start]);
    }
}
vector<vector<int> > permute(vector<int> &A) {
    vector<vector<int> > result;
    permutations(A, 0, result);
    return result;
}

//Using next_permutation function
void permute(string str) {
    // Sort the string in lexicographically ascennding order
    sort(str.begin(), str.end());

    // Keep printing next permutation while there is next permutation
    //If final permutation is reached then it returns false
    do {
        cout << str << endl;
    } while (next_permutation(str.begin(), str.end()));
}

int main() {
    // permutations("ACBC");
    permutations("AAB");
}