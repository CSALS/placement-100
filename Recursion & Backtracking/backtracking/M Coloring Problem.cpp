#include <bits/stdc++.h>
using namespace std;

//https://practice.geeksforgeeks.org/problems/m-coloring-problem/0

int n, m, e;

bool isSafe(int u, int col, vector<int> adj_list[], vector<int> color) {
    for (int v : adj_list[u]) {
        if (color[v] == col) return false;
    }
    return true;
}

bool backtrack(int u, vector<int> adj_list[], vector<int> &color) {
    if (u >= n) {
        return true;
    }

    for (int col = 0; col < m; ++col) {
        if (isSafe(u, col, adj_list, color)) {
            color[u] = col;  //make a choice
            if (backtrack(u + 1, adj_list, color)) return true;
            color[u] = -1;  //undo the choice
        }
    }
    return false;
}

int solution() {
    cin >> n >> m >> e;
    vector<int> adj_list[n];
    while (e--) {
        int u, v;
        cin >> u >> v;
        adj_list[u - 1].push_back(v - 1);
        adj_list[v - 1].push_back(u - 1);
    }
    vector<int> color(n, -1);
    //
    return backtrack(0, adj_list, color);
}