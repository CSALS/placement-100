#include <bits/stdc++.h>
using namespace std;

/**
 * link to question : https://leetcode.com/problems/generate-parentheses/
 * 3 Keys of backtracking
 * 1. our choice = place ( or )
 * 2. our constraints = CANT CLOSE UNTIL WE OPEN. So count of ( matters
 * 3. our goal = need 2*N placements
 * Time Complexity = O(2^N) = loose bound. we use pruning
 */

vector<string> generateParenthesis(int n) {
    string solution;
    vector<string> result;
    generateParenthesisUtil(result, n, solution);
    return result;
}

void generateParenthesisUtil(vector<string> &result, int n, string solution, int openingCount = 0, int closingCount = 0) {
    //our goal
    if (openingCount == n && closingCount == n) {
        result.push_back(solution);
        return;
    }

    /*
        our choice = place ( or )
        our constraint on that choice = 
        place ( when opening < n
        place ) only when opening > closing (there are more open than close)
    */
    if (openingCount < n) {
        solution.push_back('(');  //place that choice
        generateParenthesisUtil(result, n, solution, openingCount + 1, closingCount);
        solution.pop_back();  //undo that choice
    }
    if (openingCount > closingCount) {
        solution.push_back(')');  //place that choice
        generateParenthesisUtil(result, n, solution, openingCount, closingCount + 1);
        solution.pop_back();  //undo that choice
    }
}
