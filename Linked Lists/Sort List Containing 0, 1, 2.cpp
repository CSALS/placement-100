#include "LinkedList.h"

/**
 * https://practice.geeksforgeeks.org/problems/given-a-linked-list-of-0s-1s-and-2s-sort-it/1
 * Handle corner cases properly
 */
void update(Node *&head, Node *&tail, Node *curr) {
    if (head == NULL) {
        head = curr;
        tail = curr;
    } else {
        tail->next = curr;
        tail = curr;
    }
}

Node *segregate(Node *head) {
    Node *zeroHead = NULL, *zeroTail = NULL;
    Node *oneHead = NULL, *oneTail = NULL;
    Node *twoHead = NULL, *twoTail = NULL;

    Node *curr = head;
    while (curr != NULL) {
        if (curr->val == 0) {
            update(zeroHead, zeroTail, curr);
        } else if (curr->val == 1) {
            update(oneHead, oneTail, curr);
        } else {
            update(twoHead, twoTail, curr);
        }
        curr = curr->next;
    }
    if (zeroTail)
        zeroTail->next = NULL;
    if (oneTail)
        oneTail->next = NULL;
    if (twoTail)
        twoTail->next = NULL;
    if (!zeroTail) {
        if (oneTail == NULL) {
            head = twoHead;
        } else {
            oneTail->next = twoHead;
            head = oneHead;
        }
    } else {
        if (oneTail) {
            zeroTail->next = oneHead;
            oneTail->next = twoHead;
        } else {
            zeroTail->next = twoHead;
        }
        head = zeroHead;
    }
    return head;
}