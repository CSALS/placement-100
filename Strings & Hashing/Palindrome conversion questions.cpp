#include <bits/stdc++.h>
using namespace std;

/**
 * TODO: https://www.geeksforgeeks.org/palindrome-by-swapping-only-one-character/
 * Palindrome by swapping only one character
 */

/**
 * Convert the string into palindrome string by changing only one character.
 * Two pointers i = 0 and j = n -1. Iterate and count mismatches
 * If mismatch count <= 1 then it is possible
 * else Not Possible
 */

/**
 * Check if characters of a given string can be rearranged to form a palindrome
 * Find frequency count of chars of the string
 * Count the number of chars having odd freq.
 * if that count <= 1 then can form palindrome
 * else cannot
 */

/**
 * https://www.interviewbit.com/problems/convert-to-palindrome/
 * Given a string A consisting only of lowercase characters,
 * we need to check whether it is possible to make this string a palindrome after removing exactly one character from this.
 */
bool isPalindrome(string s, int low, int high) {
    while (low < high) {
        if (s[low] != s[high]) return false;
        low++;
        high--;
    }
    return true;
}

int solve(string str) {
    int n = str.length();
    int low = 0, high = n - 1;
    while (low < high) {
        if (str[low] == str[high]) {
            low++;
            high--;
        } else {
            //Remove str[low] and check if it is palindrome
            if (isPalindrome(str, low + 1, high))
                return true;
            //Remoev str[high] and check if it is palindrome
            if (isPalindrome(str, low, high - 1))
                return true;
            return false;
        }
    }
    //String is palindrome
    //In odd length, remove middle element
    //In even length, remove one of middle element
    return true;
}

/**
 * IMP
 * https://leetcode.com/discuss/interview-question/351783/Microsoft-or-Online-Assessment-or-Minswaps
 * minimum adjacent swaps to make a string into palindrome
 */

bool canBePalindrome(string s) {
    unordered_map<char, int> freqMap;
    for (char c : s)
        freqMap[c]++;

    int oddCount = 0;
    for (auto it : freqMap) {
        if (it.second & 1) {
            oddCount++;
        }
    }
    if (oddCount > 1) return false;
    return true;
}

void swap(string &s, int i, int j, int &swapCount) {
    char temp = s[i];
    s[i] = s[j];
    s[j] = temp;
    swapCount++;
}

int minSwaps(string s) {
    //Check if this can form a palindrome or not
    if (!canBePalindrome(s))
        return -1;

    //Use two pointers one at 0 and one at n-1. if both are same then just shrink them
    //If not same then at second ptr keep swapping till you get correct character

    int i = 0, j = s.length() - 1;
    int swapCount = 0;
    while (i < j) {
        //Just shrink the window
        if (s[i] == s[j]) {
            i++;
            j--;
            continue;
        }

        int k = j - 1;
        while (s[k] != s[i]) {
            k--;
        }
        if (k == i) {
            //For s[i] there's no matching character found in [i+1...j]
            //swap it with adjacent char and don't change pointers
            swap(s, k, k + 1, swapCount);
        } else {
            //Now s[k] == s[i] and swap till it gets to j
            for (int ind = k + 1; ind <= j; ++ind) {
                //swap s[ind] and s[ind-1]
                swap(s, ind - 1, ind, swapCount);
            }
            i++;
            j--;
        }
    }
    return swapCount;
}