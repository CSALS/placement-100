/**
 * https://www.geeksforgeeks.org/check-if-two-strings-can-be-made-equal-by-swapping-one-character-among-each-other/?ref=rp
 * Traverse the string and find the mismatching characters
 * If mismatch count == 1 or > 2 then it is not possible
 * If it is 0 then already equal
 * If it is 2 then try to swap the elements and check if equal
 */