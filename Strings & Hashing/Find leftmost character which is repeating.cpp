#include <bits/stdc++.h>
using namespace std;

/**
 * Input charan then answer is 3
 * Input geeksforgeeks then answer is 0
 * 1. Using 2 traversals (one for maintaining freq map. other for printing the left most index whose freq > 1)
 * 2. Using 1 traversal (maintain firstIndex[256])
 */

int leftMost1(string str) {
    int n = str.length();
    int firstIndex[256];
    memset(firstIndex, -1, sizeof(firstIndex));
    int res = INT_MAX;
    for (int i = 0; i < n; ++i) {
        char c = str[i];
        if (firstIndex[c] == -1)
            firstIndex[c] = i;
        else {
            res = min(res, firstIndex[c]);
        }
    }
    return (res == INT_MAX) ? -1 : res;
}

//Without using min comparision
int leftMost2(string str) {
    int n = str.length();
    int firstIndex[256];
    memset(firstIndex, -1, sizeof(firstIndex));
    int res = -1;
    for (int i = n - 1; i >= 0; --i) {
        char c = str[i];
        if (firstIndex[c] == -1)
            firstIndex[c] = i;
        else {
            res = i;
        }
    }
    return res;
}

int main() {
    cout << leftMost2("charan") << endl;         //2
    cout << leftMost2("geeksforgeeks") << endl;  //0
}