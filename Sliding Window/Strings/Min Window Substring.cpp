#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/minimum-window-substring/
 * Given a string S and a string T, find the minimum window in S which will contain all the characters in T in complexity O(n).
 */
class Solution {
   public:
    string minWindow(string S, string T) {
        if (S.empty() || T.empty() || T.length() > S.length())
            return "";
        unordered_map<char, int> T_freq;
        for (char c : T)
            T_freq[c]++;

        int expectedMatchings = T.length();
        int matchings = 0;  //number of characters in window matched in T
        int left = 0, right = 0;
        int finalStart = -1, finalSize = INT_MAX;
        unordered_map<char, int> S_freq;
        while (right < S.length()) {
            char c = S[right];
            S_freq[c]++;
            if (T_freq.find(c) != T_freq.end() && S_freq[c] <= T_freq[c])
                matchings++;

            if (matchings == expectedMatchings) {
                //[left....right] contains the string T. try to minimize the window
                char c = S[left];
                //If S[left] is either not present in T or the window has more than required characters
                while (left <= right && (T_freq.find(c) == T_freq.end() || S_freq[c] > T_freq[c])) {
                    S_freq[c]--;
                    left++;
                    c = S[left];
                }
                int windowSize = right - left + 1;
                if (finalSize > windowSize) {
                    finalSize = windowSize;
                    finalStart = left;
                }
            }
            right++;
        }
        if (finalStart == -1)
            return "";
        else
            return S.substr(finalStart, finalSize);
    }
};