#include <bits/stdc++.h>
using namespace std;

//https://leetcode.com/problems/substring-with-concatenation-of-all-words/
//https://www.interviewbit.com/problems/substring-concatenation/

class Solution {
   public:
    vector<int> findSubstring(string s, vector<string>& words);
};

vector<int> Solution::findSubstring(string text, vector<string>& words) {
    if (text.empty() || words.empty())
        return {};
    unordered_map<string, int> wordMap;
    for (string word : words)
        wordMap[word]++;
    int wordLength = words[0].length();
    int offset = words.size() * wordLength;  //each word has same length
    vector<int> result;
    for (int startIndex = 0; startIndex < text.length(); ++startIndex) {
        int endIndex = startIndex + offset - 1;
        if (endIndex >= text.length())
            break;
        //Check if all words of length wordLength in [startIndex.....endIndex] present in map
        bool isSubstring = true;
        unordered_map<string, int> alreadyMapped;
        for (int i = startIndex; i <= endIndex; i += wordLength) {
            string word = text.substr(i, wordLength);
            if (wordMap.find(word) == wordMap.end()) {
                isSubstring = false;
                break;
            } else if (alreadyMapped[word] + 1 > wordMap[word]) {
                isSubstring = false;
                break;
            } else {
                alreadyMapped[word]++;
            }
        }
        if (isSubstring)
            result.push_back(startIndex);
    }
    return result;
}
