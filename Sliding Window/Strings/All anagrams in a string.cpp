#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/find-all-anagrams-in-a-string
 * Given a string s and a non-empty string p, find all the start indices of p's anagrams in s.
 */

class Solution {
   public:
    vector<int> findAnagrams(string S, string T) {
        if (T.length() > S.length())
            return {};
        vector<int> result;
        unordered_map<char, int> T_freq;
        for (char c : T)
            T_freq[c]++;

        int left = 0, right = 0;
        unordered_map<char, int> S_freq;
        int matchings = 0;
        while (right < S.length()) {
            char c = S[right];
            S_freq[c]++;
            if (T_freq.find(c) != T_freq.end() && S_freq[c] <= T_freq[c])
                matchings++;
            int windowSize = right - left + 1;
            if (windowSize == T.length()) {
                if (matchings == windowSize) {
                    result.push_back(left);
                }
                char c = S[left++];
                S_freq[c]--;
                if (T_freq.find(c) != T_freq.end() && S_freq[c] < T_freq[c])
                    matchings--;
            }
            right++;
        }
        return result;
    }
};