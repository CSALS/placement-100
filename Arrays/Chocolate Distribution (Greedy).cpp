#include <bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/chocolate-distribution-problem/
int solve(vector<int> &arr, int M) {
    sort(arr.begin(), arr.end());
    int i = 0, j = M - 1;
    int min_diff = INT_MAX;
    while (j < arr.size()) {
        min_diff = min(min_diff, arr[j++] - arr[i++]);
    }
    return (min_diff == INT_MAX) ? 0 : min_diff;
}
