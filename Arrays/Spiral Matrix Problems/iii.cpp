#include <bits/stdc++.h>
using namespace std;

/**
 * IMPPP
 * https://leetcode.com/problems/spiral-matrix-iii/
 * See the question for clarity
*/

/**
 * Approach = visting order
 * 1 steps Right, 1 steps Down, 2 steps left, 2 steps up, 3 steps right, 3 steps down ...
 */
class Solution {
   public:
    vector<vector<int>> spiralMatrixIII(int R, int C, int r0, int c0) {
        int currX = r0, currY = c0;
        vector<vector<int>> result = {{currX, currY}};
        int visitedCount = 1;
        /**
         * 0. Left to right
         * 1. top to bottom
         * 2. right to left
         * 3. bottom to top
         */
        int direction = 0;
        vector<vector<int>> offset = {
            {0, 1}, {1, 0}, {0, -1}, {-1, 0}};
        int stepsCount = 0;
        while (visitedCount < R * C) {
            if (direction == 2 || direction == 0)
                stepsCount++;
            for (int i = 0; i < stepsCount; ++i) {
                currX = currX + offset[direction][0];
                currY = currY + offset[direction][1];
                //Ignore out of bound values. Just continue. We will eventually come back in the grid
                if (currX < R && currY < C && currX >= 0 && currY >= 0) {
                    result.push_back({currX, currY});
                    visitedCount++;
                }
            }
            direction = (direction + 1) % 4;
            // if(dir == 0) {
            //     for(int i = 0; i < steps; ++i) {
            //         c0++;
            //         if(r0 < R && r0 >= 0 && c0 < C && c0 >= 0) {
            //             result.push_back({r0, c0});
            //         }
            //     }
            //     dir = 1;
            // }
            // else if(dir == 1) {
            //     for(int i = 0; i < steps; ++i) {
            //         r0++;
            //         if(r0 < R && r0 >= 0 && c0 < C && c0 >= 0) {
            //             result.push_back({r0, c0});
            //         }
            //     }
            //     dir = 2;
            //     steps++;
            // }
            // else if(dir == 2) {
            //     for(int i = 0; i < steps; ++i) {
            //         c0--;
            //         if(r0 < R && r0 >= 0 && c0 < C && c0 >= 0) {
            //             result.push_back({r0, c0});
            //         }
            //     }
            //     dir = 3;
            // }
            // else if(dir == 3) {
            //     for(int i = 0; i < steps; ++i) {
            //         r0--;
            //         if(r0 < R && r0 >= 0 && c0 < C && c0 >= 0) {
            //             result.push_back({r0, c0});
            //         }
            //     }
            //     dir = 0;
            //     steps++;
            // }
        }
        return result;
    }
};