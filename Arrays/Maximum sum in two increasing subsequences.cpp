#include <algorithm>
#include <iostream>
#include <vector>
using namespace std;
#define int long long

//https://www.spoj.com/problems/ANARC05B/

//Idea = whenever a intersection occurs , use prefix sum arrays to find the maximum of which of the two arrays add maximum to the sum
int maxSum(vector<int> &arr1, int n1, vector<int> &arr2, int n2) {
    auto getPrefixSum = [&](vector<int> &arr, int n) {
        vector<int> prefix_sum(n, arr[0]);
        for (int i = 1; i < n; ++i)
            prefix_sum[i] = prefix_sum[i - 1] + arr[i];
        return prefix_sum;
    };
    //sum[i...j] in array
    auto getSumInRange = [&](vector<int> &prefix_sum, int i, int j) {
        int sum = prefix_sum[j];
        if (i != 0) sum -= prefix_sum[i - 1];
        return sum;
    };

    vector<int> prefix_sum1 = getPrefixSum(arr1, n1);
    vector<int> prefix_sum2 = getPrefixSum(arr2, n2);
    //Iterate over one array and check if the current element is present in other array (use binary search since sorted)
    int max_sum = 0;
    int start1 = 0, start2 = 0;
    for (int i = 0; i < n2; ++i) {
        // if (!binary_search(arr1.begin(), arr1.end(), arr2[i])) continue;
        int index = lower_bound(arr1.begin(), arr1.end(), arr2[i]) - arr1.begin();
        if (index == n1) continue;
        if (arr1[index] != arr2[i]) continue;

        //arr2[i] is intersecting with arr1[index]
        //Find max of sum[start1....index] and sum[start2....i]
        int sum1 = getSumInRange(prefix_sum1, start1, index);
        int sum2 = getSumInRange(prefix_sum2, start2, i);
        max_sum += max(sum1, sum2);
        start1 = index + 1;
        start2 = i + 1;
    }
    max_sum += max(getSumInRange(prefix_sum1, start1, n1 - 1), getSumInRange(prefix_sum2, start2, n2 - 1));
    return max_sum;
}

int32_t main() {
    while (true) {
        int n1;
        cin >> n1;
        if (n1 == 0) return 0;

        vector<int> arr1(n1);
        for (int &x : arr1) cin >> x;
        int n2;
        cin >> n2;
        vector<int> arr2(n2);
        for (int &x : arr2) cin >> x;

        cout << maxSum(arr1, n1, arr2, n2) << endl;
        // break;
    }
}

/*
7 3 5 7 9 20 25 30
7 1 4 7 11 14 15 25
*/