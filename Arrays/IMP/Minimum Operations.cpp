#include <bits/stdc++.h>
using namespace std;

/**
 * Minimum operation to make all elements equal in array
 * Given an array with n positive integers. We need to find the minimum number of operation to make all elements equal.
 *  We can perform addition, multiplication, subtraction or division with any element on an array element. 
 * Basically convert all elements into mode of array
 */

/**
 * Minimum Increment / decrement to make array elements equal
 * Given an array of integers. In one operation you can either Increment/Decrement any element by 1. 
 * The task is to find the minimum operations needed to be performed on the array elements to make all array elements equal.
 * Median minimizes the sum of absolute deviation.
 * So convert all elements into median.
 * If there are two medians (in case of even length array) then use both of them and find min operations out of them
 */

/**
 * https://www.geeksforgeeks.org/minimum-number-of-increment-decrement-operations-such-that-array-contains-all-elements-from-1-to-n/?ref=rp
 * Minimum number of increment/decrement operations such that array contains all elements from 1 to N
 * Simply sort the array
 * Find how many operations needed to convert arr[i] into (i + 1) [0-based indexing]

/**
 * https://www.interviewbit.com/problems/minimum-operations-of-given-type-to-make-all-elements-of-a-matrix-equal/
 * Minimum operations of given type to make all elements of a matrix equal
 * The median minimizes the the sum of absolute deviations. So make all elements equal to median
 * Median = x + count * K
 * count = abs(Median - X)/K (if not divisible then it is not possible to make all elements equal)
 */
int solve(vector<vector<int> > &A, int B) {
    int N = A.size();
    int M = A[0].size();
    vector<int> arr;
    for (int i = 0; i < N; ++i) {
        for (int j = 0; j < M; ++j) {
            arr.push_back(A[i][j]);
        }
    }
    sort(arr.begin(), arr.end());
    int median = arr[(N * M) / 2];
    int ops1 = 0;
    for (int i = 0; i < N * M; ++i) {
        if (abs(arr[i] - median) % B != 0)
            return -1;
        ops1 += abs(arr[i] - median) / B;
    }

    //If number of elements are even then consider both the medians and take min of them
    int ops2 = INT_MAX;
    if ((N * M) % 2 == 0) {
        median = arr[(N * M) / 2 - 1];
        for (int i = 0; i < N * M; ++i) {
            if (abs(arr[i] - median) % B != 0)
                return -1;
            if (ops2 == INT_MAX) {
                ops2 = abs(arr[i] - median) / B;
            } else {
                ops2 += abs(arr[i] - median) / B;
            }
        }
    }
    return min(ops1, ops2);
}
