#include <bits/stdc++.h>
using namespace std;

//O(N) Time | O(N) Space
void NearestGreaterToRight(vector<int> arr) {
    int NGR[arr.size()];
    stack<int> st;
    for (int i = arr.size() - 1; i >= 0; --i) {
        while (!st.empty() && arr[i] >= st.top())
            st.pop();
        NGR[i] = st.empty() ? -1 : st.top();
        st.push(arr[i]);
    }
}