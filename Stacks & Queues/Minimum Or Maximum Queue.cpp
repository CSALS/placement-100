#include <bits/stdc++.h>
using namespace std;

/**
 * Idea is to have a deque along with the normal queue.
 * For minimum queue deque will be storing elements in increasing order.
 * For maximum queue maintain in decreasing order.
 */

queue<int> Q;
deque<int> D;  //D.front() always has minimum element

void push(int data) {
    Q.push(data);

    //Pop all elements greater than the current element since data will be new minimum since it is at the last in the queue and those elements will be popped first (FIFO)
    while (!D.empty() && D.back() > data)
        D.pop_back();

    D.push_back(data);
}

int get_min() {
    return D.front();
}

void pop() {
    int data = Q.front();
    Q.pop();

    if (data == D.front())
        D.pop_front();
}