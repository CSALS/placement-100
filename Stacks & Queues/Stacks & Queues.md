#
TIP (**IMP**)
0. expressions, paranthesis, balanced etc. => stack 
1. If for an element you need to examine right sub array then start the loop from right to left
else start the loop from left to right
2. You either store indexes (to get count as result) or actual element (to get actual element as result)

Questions Group
1. Nearest Greater To Left (NGL), NGR, NSL, NSR
2. Stock Span Problem
3. Max Area Of Histogram
4. (**IMP**)Max Area Of Rectangle In Binary Matrix [DP Approach, Using Max Area of histogram]
5. (**IMP**)Rain Water Trapping (Try O(1) Two Pointers Approach)

Design Questions
1. Min Stack (With & Without Extra Space)
2. Implement Stack Using Heap
3. Implement Stack Using Queue
4. Implementing queue from two stacks

Paranthesis/Expressions Related
1. (**IMP**)Infix, Prefix, Postfix (conversions)
2. https://www.interviewbit.com/problems/redundant-braces/
3. https://www.interviewbit.com/problems/minimum-bracket-reversals/ = 
thought of graph approach with bfs for getting min reversals.... but that gives exponential complexity.
so whenever expression of (, ) + balanced is given = try to just think of using stacks somehow even if it is min or max etc.
4. https://www.interviewbit.com/problems/decode-string/ something parenthesis related try to relate to stacks behaviour
5. https://www.interviewbit.com/problems/longest-valid-parentheses/ [DP , Stack Approach]

Misc
1. https://www.interviewbit.com/problems/simplify-directory-path/ (lot of cases)
5. If asked to use constant space but there is no way to do that then think of using recursion stack space
* https://www.geeksforgeeks.org/delete-middle-element-stack/ (Do it without extra space. So use recursion stack space )
