#include <bits/stdc++.h>
using namespace std;

bool isPossible(vector<int> &pages, int studentsRequired, int maxPages) {
    /*
        maxPages => each student pages allotted shouldn't exceed this
        Find how many students you can allot in such answer
        If that studentCount <= studentsRequired then true else false
        Why <= and not ==?
        Because we are not actually allotting books to students. We are just checking
        if it is possible to not exceed maxPages limit for each student and not exceed studentsRequired.
        What if studentsPossible = 4 and studentsRequired = 5 and function returns true and say we don't find
        another answer in high = mid - 1 search space. Isn't the answer wrong?
        NOPE. Think carefully.
        It is possible to split the pages obtained of one student and give it to another student and still max. pages
        will remain same since we are only splitting one guy.
        What if studentsPossible = 1 and studentsRequired = 2?
        Then now max. pages allotted decreases and that means we should be able to find answer in high = mid - 1 search space
    */
    int studentsPossible = 1;
    int pagesCount = 0;
    for (int i = 0; i < pages.size(); ++i) {
        if (pages[i] > maxPages)
            return false;
        if (pagesCount + pages[i] <= maxPages) {
            pagesCount += pages[i];
        } else {
            studentsPossible++;
            pagesCount = pages[i];
            if (pagesCount > maxPages)
                return false;
            if (studentsPossible > studentsRequired)
                return false;
        }
    }
    return studentsPossible <= studentsRequired;
}

int books(vector<int> &pages, int students) {
    if (pages.size() < students)
        return -1;
    /*
        Minimize "max pages alloted to a student" based on some constraint 
                                    (each student gets atleast one book, contigous order)
        We will use binary search on answer
        where answer is "max pages alloted to a student"
    */
    int low = 0;
    int high = 0;
    for (int page : pages)
        high += page;

    int ans = INT_MAX;  //since we need min answer
    while (low <= high) {
        int mid = (low + high) / 2;
        if (isPossible(pages, students, mid)) {
            ans = min(ans, mid);
            high = mid - 1;  //go left to get a more smaller answer
        } else {
            low = mid + 1;
        }
    }
    return ans == INT_MAX ? -1 : ans;
}
