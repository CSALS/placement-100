# When to Use
- When you are using hashing on strings it's better to use trie on those strings.
- Useful for prefix search. Hashing doesn't support it

# Applications
Implementing dictionary, auto-complete feature

# Binary Tree vs Trie
- In binary tree if we want empty tree then it is just root = NULL. No memory is allocated
- In trie if we want empty trie then we need one root node for sure which has all child as NULL. Memory is allocated

# Hashing vs Trie

- https://stackoverflow.com/questions/245878/how-do-i-choose-between-a-hash-table-and-a-trie-prefix-tree

- search, insert, delete
Trie =     O(word_length) in worst case
Hashing =  O(word_length) to compute hash + O(1) lookup time in avg = O(word_length) in average case
- Prefix search
Trie = O(prefix_length + output_length)
Hashing = Not supported
- Lexicographic ordering
Trie = O(output_length)
Hashing = Not supported

- If in set of words there are lot of prefixes then space is shared
- It all depends on what problem you're trying to solve. 
- If all you need to do is insertions and lookups, go with a hash table. 
- If you need to solve more complex problems such as prefix-related queries, then a trie might be the better solution.


# Problems

- https://www.geeksforgeeks.org/implement-a-dictionary-using-trie/
For each node also store (string meaning)
Hashtable will waste space since in dictionary there will be lot of common prefixes

- Count distinct rows in a binary matrix
Convert row into string and hash it.
Strings + hashing => use tries
Trie {child[0, 1]}. Since size of each row string is same no need of isEnd
Insert each row in the Trie. If the row is already there, don’t print the row. If the row is not there in Trie, insert it in Trie and print it.

- https://www.interviewbit.com/problems/shortest-unique-prefix/?ref=random-problem


- Autocomplete Implementation = https://www.interviewbit.com/problems/design-search-typeahead/
Store frequence of search term at the end node. so instead of boolean isEnd, maintain int wordCount.
But finding the top 5 frequent words is time-taking since need to scan lot of nodes. Instead store the top 5 words at the node itself
which results in lower latency. Although storage is increased but is cheap so we can always tradeoff storage with latency.
https://www.geeksforgeeks.org/auto-complete-feature-using-trie/

# Reads
- https://leetcode.com/discuss/general-discussion/680706/article-on-trie-general-template-and-list-of-problems
(Solve problems in this link)
- max xor of two numbers in array