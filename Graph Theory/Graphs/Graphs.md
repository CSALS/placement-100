## **GRAPHS**

Tips = 
- Assume this unless specified = Graph may be disconnected.
- entry/exit time = in_time/out_time
- shortest path from start to end is same as end to start (for UNDIRECTED graphs only)
(do bfs/dijkstra/.. from start to end is same as end to start)

1. BFS (Directed & Undirected)
- For calculating length of shortest path from the source vertex use **dist[]** array or using **pair<node, dist> for queue**
- For tracing the actual shortest path use **parent[]** array which stores for each vertex the vertex from which we reached it.
- If you need multiple SHORTEST paths then for each index store vector of parents.
- (**IMP**)Finding all shortest paths between source & destination (Word Ladder 2)
- (**IMP**) Find the shortest path of even length from a source vertex s to a target vertex t in an unweighted graph
    (For every edge (u, v) add (u, 0) - (v, 1) and (u, 1) - (v, 0) edges, (u,0)-(u,1) and (v,0)-(v,1). 
    BFS on (s, 0) to (t, 0)). Dist parity is 0 if it is even else is 1 if it is odd.
- Lemma => During the execution of BFS, the queue holding the vertices only contains elements from at max two successive levels of the BFS tree (Used in 0-1 BFS)
- Multi-Source BFS (Kahn's Algorithm, Rotten Oranges)
Problems =
- https://www.geeksforgeeks.org/multi-source-shortest-path-in-unweighted-graph/
- https://www.geeksforgeeks.org/minimum-time-required-so-that-all-oranges-become-rotten/
- (**IMP**)https://www.geeksforgeeks.org/distance-nearest-cell-1-binary-matrix/ (think of this as variation of rotten oranges)
Manhattan Distance (|x1-x2| + |y1-y2|) b/w 2 cells is nothing but path length in bfs tree.
- Bi-Directional BFS (Finding Path from source to destination)
When To Use = Both initial and goal states are unique and completely defined.
- https://www.interviewbit.com/problems/word-ladder-i/
- Reverse BFS = bfs from goal to start. (useful if multiple sources are present) (Codechef H1)

Misc =
- https://www.geeksforgeeks.org/snake-ladder-problem-2/

TYPES of bfs => <br>
- bfs using dist[] + parent[] for tracing single path, vector of parents for tracing multiple paths
- multi-source bfs
- 0-1 bfs
- bi-directional bfs
- reverse bfs

2. DFS (Directed & Undirected)
- Finds lexicographic first path from source vertex to each vertex.
The edges in adj_list should be in sorted order(If not sorted then sort it.) And then normal dfs would suffice
- Finds shortest path in a tree (since there will be only simple path) but not in graphs.
- DFS using intime, outtime, color (0 = unvisited, 1 = entered, 2 = exited)
- (**IMP**)ancestor-descendant relationship in a tree = 
1. Using dfs + time , u is ancestor of v if => in_time[u] < in_time[v] && out_time[u] > out_time[v]
2. LCA(u, v) = u then u is ancestor of v | LCA(u, v) = v then v is ancestor of u

Misc =
- https://www.geeksforgeeks.org/count-possible-paths-two-vertices/ (Do DFS for all paths)

3. Cycles
Detection (useful for deadlock detection)
- Undirected Graph = 
    1. same method in both bfs & dfs. if visiting a node which is already visited (vis[v] = true) and it is not parent of u then there is a cycle.
    2. using DSU. we get O(ElogV + V) in worst case | O(E + V) amortized time
       but the adv. of dsu is if the edges are being added dynamically we can use this
- Directed Graph =
    1. DFS = Using back-edge concept by maintaining rec_st[] array
    If we are at a node u and we have an edge from u to v but v has been entered but not exited (that means it is currently in recursion stack) which
    means u->v is back-edge and cycle is present
    2. BFS = Kahn's Algorithm (Using indegree concept)

DFS = Coloring Method (for both undirected and directed) [DFS(Time&Color).cpp]

Shortest Cycle = <br>
1. In undirected, unweighted graph = for every vertex find all the cycles the node is present in. when you detect compute the length of that cycle.
Since src node is in cycle. dist[u] = src-------u + dist[v] = src----------v + 1 = u------v (cycle length). 
(https://www.geeksforgeeks.org/shortest-cycle-in-an-undirected-unweighted-graph/)
    Do BFS on every vertex. Use dist[v] = vertex------v distance

2. In undirected, weighted graph = for every edge (u, v) remove it from graph then do dijkstra's and find min(dist[u, v] + wt[u, v])

Printing cycles = <br>
1. Printing all cycles in undirected graph (may not work for composite cycles)

4. Topological Sorting (Only for DAG Directed Acyclic Graph)
- DFS = Gives result in descending order of exit time. 
    First do dfs to store nodes in stack in the descending order of exit time (whichever exists last will be on top)
    Do complete dfs using the stack
- Kahn's Algorithm (BFS) = Done according to indegree
- (**IMP**)Kahn's (using min heap to always get smallest value node in a topo order level) = smallest lexicographic topo order
Time Complexity :- VlogV + E. 
Each node will be pushed only once & popped only once. (insert, pop happens 2V times) but entire loop runs E times (e1 + e2 + e3 + ...). So overall is VlogV + E.
Although in the for(int v : adj[u]) we push the node but we don't push in every iteration. we only push node if it is not pushed before.
so each node is pushed once and popped once in the entire BFS. So VlogV and that for(int v : adj[u]) contributes E
- https://www.geeksforgeeks.org/all-topological-sorts-of-a-directed-acyclic-graph/

5. Articulation Points & Bridges (Connected Undirected Graph)
O(V + E) one dfs only
- Using entry time, back edges concept.
- Exploring DFS Tree.

6. Connected Components
- Undirected Graph = Using BFS, DFS
- Directed Graph = Strongly Connected Components (SCC = maximal connected subgraph)
    1. Kosaraju's Algo = 2 DFS O(V + E)
    ```
    1. 1st find order of vertices in decreasing order of their finish time (using stack)
    2. reverse edges of graph (graph transpose)
    3. then do dfs from each node in the finish time decreasing order. all nodes reachable are part of one component. (just like normal undirected graph method but here order of doing dfs is according to the stack)
    ```
    2. Tarjan's Algo = 1 DFS O(V + E)

7. (**IMP**)Shortest Paths (Getting shortest distance & shortest path tracing)

For both directed and un-directed graphs.
We do relaxing of adjacent vertices in these algos.

Single-Source => 
Shortest path distance from one node to all other nodes, shortest path tracing, -ve weight cycle detection & printing that cycle
    - Unweighted Graph = BFS O(E)               [Adj.List]
    - Weighted Graph (0 and 1/x>0) = 0-1 BFS    [Adj.List]
    - Weighted DAG = Topological Sort O(V + E)  [Adj.list]
    -h +ve Weighted Grap = Dijkstra's O((V + E) * logV) Sparse graphs | O(V^2 + E) Dense graphs [Adj.list]
    - Both +ve & -ve weighted graph (No -ve weight cycle) = Bellman Ford O(VE) [vector.of.edges]


All-pair shortest paths =>
    - +ve/-ve weighted graph = Floyd-Washall Algorithm O(V^3) [Adj.matrix]

Misc Problems =
- (**IMP**)https://www.geeksforgeeks.org/minimum-cost-path-left-right-bottom-moves-allowed/
- (**IMP**)https://www.geeksforgeeks.org/minimum-cost-path-from-source-node-to-destination-node-via-an-intermediate-node/
    For every node v store the min. of (dist(src, v) + dist(v, inter) + dist(v, dest))
        where dist(a, b) = shortest path b/w a to b
    dist(a, b) = dist(b, a) [only in case of undirected graph]
    so we need to find min of (dist(src, v) + dist(inter, v) + dist(dest, v))
    Pre-compute shortest dist using 3 dijsktra's from src, inter, dest

- https://www.geeksforgeeks.org/minimum-cost-path-left-right-bottom-moves-allowed/

8. Negative weight cycle detection in a graph (overall sum of the cycle comes negative)
For disconnected graph also
    - https://www.geeksforgeeks.org/detect-negative-cycle-graph-bellman-ford/
    - https://www.geeksforgeeks.org/detecting-negative-cycle-using-floyd-warshall/?ref=rp

9. Spanning Trees (Connected & Undirected Graph) [not.for.directed.graph]
                        Graph(V, E) = Spanning Tree(V, V - 1)
- (**IMP**)Minimum spanning tree is also the tree with minimum product of weights of edges. (It can be easily proved by replacing the weights of all edges with their logarithms)
- The maximum spanning tree (spanning tree with the sum of weights of edges being maximum) of a graph can be obtained similarly to that of the minimum spanning tree, by changing the signs of the weights of all the edges to their opposite and then applying any of the minimum spanning tree algorithm.
- 2nd best MST (https://cp-algorithms.com/graph/second_best_mst.html)
    In min. spanning tree T we just need to replace one edge to get 2nd best MST
    For every edge in the graph
        T_2nd = (T - old_edge U new_edge)
        this T_2nd should be spanning tree and store min. of that

- MST = Prims Algorithm (Dense) [somewhat.similar.to.dijkstras]
1. Dense Graphs + Adj. Matrix = O(V^2)
2. Sparse Graphs + Adj. List + Set = O(VlogV + ElogV)
3. With heap (updating key function supported in logV) = O(VlogV + ElogV)

- MST = Krushkal Algorithm (Sparse)
1. Simple Implementation = O(VE + ElogE)
2. Using DSU = O(ElogE + ElogV)

Prims vs Kruskal
    If in question you are getting vector of edges [(u, v, wt)] then use krushkal. for prims you need to change the structure of graph
    ```
    Use Prim's algorithm when you have a graph with lots of edges.
    For a graph with V vertices E edges, Kruskal's algorithm runs in O(E log V) time and Prim's algorithm can run in O(E + V log V) amortized time, if you use a Fibonacci Heap.
    Prim's algorithm is significantly faster in the limit when you've got a really dense graph with many more edges than vertices. Kruskal performs better in typical situations (sparse graphs) because it uses simpler data structures.

    ```

10. Disjoint-Set Union
- union by rank & find using path by compression = O(logN)
- nearly constant time on average (O(1) amortized time)
- If in question you are getting vector of edges [(u, v, wt)] then better to use DSU 
    https://www.codechef.com/problems/CD1IT5

Applications =
0. connected components (obvious application)
1. krushkal 
2. cycle detection (undirected graph)
    ```
        for(int u = 0; u < V; ++u) {
            for(int v : adj[u]) {
                //u----v edge
                if(find(u) == find(v)) {
                    //u and v already have a path. so adding them will lead to cycle
                    return true
                }
                union(u, v)
            }
        }
        return true
    ```
    worst case => O(ElogV + V)
    amortizde => over E operations it is O(E + V) Time



11. Bipartite Graph
- graph can be divided into sets such that there is no edge b/w two nodes of same set.
  edges are always from one set to another.
- To check if graph is bipartite use this = Bipartite graph should be 2-colorable 
- If graph shouldn't have odd-length cycle = then it should be bipartite
- (**IMP**)https://www.geeksforgeeks.org/check-if-there-is-a-cycle-with-odd-weight-sum-in-an-undirected-graph/?ref=rp


# REF

- https://www.geeksforgeeks.org/minimum-steps-needed-to-cover-a-sequence-of-points-on-an-infinite-grid/
    Can easily confuse this for as graph problem and use bfs.
    But since every direction movement is allowed
    min. steps to go from (x1, y1) -> (x2, y2) is max(|x1-x2|, |y1-y2|) //horizontal dist, vertical dist

- You will be getting a stream of edges for a directed graph. You have to tell at what point the cycle is formed. (My approach was using Disjoint sets ). He asked if I can implement the same algorithm to the undirected graph also? He asked me to implement Disjoint sets algorithm and to make it efficient using union by rank and path compression.

# TODO
- Snake and ladder https://practice.geeksforgeeks.org/problems/snake-and-ladder-problem/0/
- https://www.youtube.com/watch?v=OutDY_ICb80 (snake ladder bfs)




# TODO
https://www.lintcode.com/en/old/problem/number-of-distinct-islands-ii/ (follow up to number of islands bfs)
complete graphs in IB
https://www.interviewbit.com/problems/permutation-swaps/
https://www.geeksforgeeks.org/two-clique-problem-check-graph-can-divided-two-cliques/
tree vertex splitting problem