#include <bits/stdc++.h>
using namespace std;

int V;
int adj[V][V] = {0};         // adj[i][j] = 0 then no edge
const int INF = 1000000000;  // weight == INF => No edge

/*
    The outline of this algo. is same as dijkstras
    In dijkstra's we used dist[u] = which denotes dist of source node to u
    Here we use key[v] = which denotes EDGE weight with its parent 
    When this node is included in MST_SET it has a parent. that edge weight is stored here. 
    if we get smaller weight then we can replace
*/

// 1. Dense Graphs + Adjcaceny Matrix Rep. = O(V^2)
int minKey(int key[], bool MST_Set[]) {
    int min_key = -1;
    int min_value = INF;
    for (int u = 0; u < V; ++u) {
        if (MST_Set[u] == false && key[u] <= min_value) {
            min_key = u;
            min_value = key[u];
        }
    }
    return min_key;
}

void prims1() {
    int key[V] = {INF};
    int parent[V] = {-1};
    bool MST_Set[V] = {false};
    int MST_Edges = V - 1;
    int min_cost = 0;
    // choosing 1st node as 0
    key[0] = 0;
    parent[0] = -1;
    while (MST_Edges--) {
        // 1. Pick min key vertex not in the mst set
        int u = minKey(key, MST_Set);
        if (key[u] == INF) {
            cout << "No MST. Not connected graph.\n";
            exit(0);
        }
        // 2. Include this node in MST Set
        MST_Set[u] = true;
        min_cost += key[u];

        // 3. Relax all adjacent vertices not in mst set
        for (int v = 0; v < V; ++v) {
            if (adj[u][v] != 0 && MST_Set[v] == false && key[v] > adj[u][v]) {
                key[v] = adj[u][v];
                parent[v] = u;
            }
        }
    }

    /* MST Edges tracing
        for each node except 0 add (parent[v], v) to MST Edges Result
        for(int i=1;i<V;++i) print({parent[v], v})
    */
}

// 2. Sparse Graphs + Adj. List Rep + Set = O(VlogV + ElogV)
// Can use heap also. But C++ prioirity queue doesn't support updates. So time
// will become O(VlogE + ElogE) but you can use custom made heap with support
// for updating key value which can be done in O(logV) if all indexes of keys
// are stored then time will be O(VlogV + ElogV)
vector < vector<pair<int, int>> adj_list;
void prims2() {
    int key[V] = {INF};
    int parent[V] = {-1};
    bool MST_Set[V] = {false};
    int min_cost = 0;
    // choosing 1st node as 0
    key[0] = 0;
    parent[0] = -1;
    set<pair<int, int>> q;  //(key value, node).
    q.insert({0, 0});
    while (!q.empty()) {
        // 1. Find min key node not in the MST (all the nodes which are in MST
        // won't be in the set)
        int u = q.begin()->second;
        if (q.begin()->first == INF) {
            cout << "No MST. Not connected graph.\n";
            exit(0);
        }
        q.erase(q.begin());

        MST_Set[u] = true;
        min_cost += key[u];

        for (auto edge : adj_list[u]) {
            int v = edge.first;
            int edge_wt = edge.second;
            if (MST_Set[v] == false && key[v] > edge_wt) {
                q.erase({key[v], v});
                key[v] = edge_wt;
                parent[v] = u;
                q.insert({key[v], v});
            }
        }
    }
}