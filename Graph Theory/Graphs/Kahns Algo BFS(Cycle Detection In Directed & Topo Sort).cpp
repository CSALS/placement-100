#include <bits/stdc++.h>
using namespace std;

bool isCyclic(int V, vector<int> adj[]) {
    vector<int> indegree(V, 0);
    for (int u = 0; u < V; ++u) {
        for (int v : adj[u]) {
            indegree[v]++;
        }
    }
    //indegree[u] = 0 means no dependency on that vertex u
    queue<int> q;
    vector<bool> visited(V, false);  // this is not needed since we visit/push node only if it's indegree is 0
    vector<int> topo_order;
    for (int u = 0; u < V; ++u) {
        if (indegree[u] == 0) {
            q.push(u);  //multi-source bfs since pushing multiple sources before doing BFS
            visited[u] = true;
        }
    }
    //number of visited nodes is tracked by count. if count != V at the end of bfs then there is cycle
    int count = 0;  // count of the nodes whose indegree became 0 / visited nodes count
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        topo_order.push_back(u);
        for (int v : adj[u]) {
            indegree[v]--;
            if (indegree[v] == 0 && !visited[v]) {
                visited[v] = true;
                q.push(v);
            }
        }
        count++;
    }
    if (count == V) {
        // No cycle
        print(topo_order);
        return false;
    } else {
        // Cycle present so no topo order present
        return true;
    }
}