#include <bits/stdc++.h>
using namespace std;

struct ListNode {
    int val;
    ListNode* next;
    ListNode(int x) : val(x), next(NULL) {}
};

struct CompareNodeValues {
    bool operator()(ListNode* const& A, ListNode* const& B) {
        //True if B is ordered before A (READ THISSS. TRUE if B should come before A)
        return B->val < A->val;
    }
};

ListNode* mergeKLists(vector<ListNode*>& headsList) {
    if (headsList.empty())
        return NULL;
    priority_queue<ListNode*, vector<ListNode*>, CompareNodeValues> minHeap;
    for (ListNode* head : headsList) {
        //Better error handling = to check if head is NULL
        if (head != NULL)
            minHeap.push(head);
    }
    ListNode *sortedHead = NULL, *sortedTail = NULL;
    while (!minHeap.empty()) {
        ListNode* node = minHeap.top();
        minHeap.pop();
        if (sortedHead == NULL) {
            sortedHead = node;
            sortedTail = node;
        } else {
            sortedTail->next = node;
            sortedTail = node;
        }
        if (node->next != NULL)
            minHeap.push(node->next);
    }
    return sortedHead;
}
