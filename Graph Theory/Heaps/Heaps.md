# Identification =
2. K + Smallest/Largest/Large/Frequent/Closest/Lowest
3. K + Top/Largest = MIN HEAP
4. K + Smallest = MAX HEAP
5. Heap size will be K
6. K + sort array = Heap
7. If instead of sorting all 'N' elements we can get result by sorting 'K' 
8. Freq + Sort = Heap 
9. NlogK = heap
Sorting Based = NlogN
Heap Based = NlogK.

# Problems = 

- For the below questions if sorting is used then we get NlogN. But using heap we get NlogK.
- To ensure NlogK is always less than NlogN, solve separately for the case K = N. Keep that as base case. Sometimes the array itself is the answer
- **Some problems can be solved using QUICK SELECT which gives linear time on average**

0. Mostly will be used in greedy problems. 

1. Merge K Sorted Lists
2. Merge K Sorted Arrays

3. https://www.interviewbit.com/problems/magician-and-chocolates/
4. https://www.interviewbit.com/problems/n-max-pair-combinations/

5. Kth Smallest/Largest and First K large/small elements [Most efficient is not using heaps but rather using QUICK SELECT algorithm]
6. Sort K-Sorted Array/Nearly-Sorted Array

7. K Closest Numbers To Given Value
- Max Heap({abs(arr[i] - value), arr[i]})
8. Top K Frequent Numbers
- Construct FreqMap first
- Min Heap({Frequency, Actual Value})
9. Frequency Sort (Array/String)
M is distinct elements
- std::priority_queue: O(N) freq_map + O(MlogM) constructing max heap {freq, value} + O(N + MlogM) extracting elements
- In case of strings max possible characters are 256. (26 if only lowercase). So it will be O(N) + O(N + 256*log256) => O(N)
If you want to preserve order of elements then store index in heap.
10. Top K frequent words. If same frequency then which ever is lexicographically smaller print it.
11. K Closest Points To Origin