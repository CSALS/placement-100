#include "../Tree.h"

/*
https://practice.geeksforgeeks.org/problems/largest-bst/1#_=_

    1.  at every node check if the current tree is bst.
        top-down approach
        O(N^2) T

    2.  bottom-up approach
        check if left subtree and right subtree are bst
        if both are bsts then check the bounds (store min, max for every tree also)

variation => https://www.geeksforgeeks.org/count-the-number-of-binary-search-trees-present-in-a-binary-tree/
    for this don't need to send size. whenever you find bst just increment result
*/
//additional information for each node
struct Info {
    bool isBST;  //is the subtree rooted at this node BST?
    int min;     //min of the entire subtree
    int max;     //max of the entire subtree
    int size;    //size of this subtree
    Info(bool isBST, int min, int max, int size) : isBST(isBST), min(min), max(max), size(size) {}

    Info(bool isBST) : isBST(isBST) {}
};
#define INF 1000000
Info largestBSTRecursive(Node *root, int &result) {
    if (!root) {
        return Info(true, INF, -INF, 0);
    }
    if (root->left == NULL && root->right == NULL) {
        return Info(true, root->data, root->data, 1);
    }

    Info left = largestBSTRecursive(root->left, result);
    Info right = largestBSTRecursive(root->right, result);

    //if both subtrees are BST then only there's a chance for
    //the current tree to be BST else it can't be BST
    if (left.isBST && right.isBST) {
        if (left.max < root->data && root->data < right.min) {
            Info current(true);
            current.size = left.size + right.size + 1;
            result = max(result, current.size);
            current.min = min(left.min, root->data);
            current.max = max(root->data, right.max);
            return current;
        }
    }
    return Info(false);
}

int largestBst(Node *root) {
    int result = 1;
    largestBSTRecursive(root, result);
    return result;
}
