#include "../Tree.h"

//https://practice.geeksforgeeks.org/problems/check-for-balanced-tree/1

int height(Node *node, bool &answer) {
    if (node == NULL) return 0;
    if (answer == false) return -1;  //cutting down uneceesary recursive calls

    int leftHeight = 1 + height(node->left, answer);
    int rightHeight = 1 + height(node->right, answer);

    if (abs(leftHeight - rightHeight) > 1) {
        answer = false;
    }

    return max(leftHeight, rightHeight);
}

bool isBalanced(Node *root) {
    if (root == NULL) return true;

    bool answer = true;
    height(root, answer);
    return answer;
}