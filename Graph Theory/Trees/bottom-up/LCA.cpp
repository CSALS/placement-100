#include "Tree.h"

Node *lcaRecursive(Node *root, int x, int y) {
    if (root == NULL)
        return NULL;

    if (root->data == x || root->data == y)
        return root;

    Node *left = lcaRecursive(root->left, x, y);
    Node *right = lcaRecursive(root->right, x, y);

    if (!left && !right)
        return NULL;
    if (left && right)
        return root;
    return (left) ? left : right;
}

bool search(Node *root, int x) {
    if (!root)
        return false;

    if (root->data == x)
        return true;
    return search(root->left, x) || search(root->right, x);
}

int lca(Node *root, int x, int y) {
    Node *lcaNode = lcaRecursive(root, x, y);
    //We need to search for both of them since if only of them is present then lcaNode will point to that node but in that case answer is -1
    //Check if both are present in tree
    if (search(lcaNode, x) && search(lcaNode, y))
        return lcaNode->data;
    return -1;
}