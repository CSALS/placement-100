#include "../../Tree.h"

/*
https://www.geeksforgeeks.org/find-distance-between-two-nodes-of-a-binary-tree/

    Draw a tree and work on the possible cases
    1. A and B are in diff. subtrees of the root
    2. A and B are in diff. subtrees but of some node which is not root [they are present in one of the subtree of root]

    Dist(A, B) = Dist(root, A) - Dist(root, lca) + Dist(root, B) - Dist(root, lca)
*/

Node* getLCA(Node* root, int x, int y) {
    if (root == NULL) return NULL;
    if (root->data == x || root->data == y) return root;
    Node* left = getLCA(root->left, x, y);
    Node* right = getLCA(root->right, x, y);
    if (left != NULL && right != NULL) {
        return root;  //root is LCA since we are going from bottom to top
    }
    if (left != NULL) {
        return left;
    }
    return right;
}

//dist from root to node containing the value key
int distFromRoot(Node* root, int key) {
    if (!root) return -1;
    if (root->data == key) return 0;
    int left = distFromRoot(root->left, key);
    if (left != -1) {
        return 1 + left;
    }
    int right = distFromRoot(root->right, key);
    if (right != -1) {
        return 1 + right;
    }
    return -1;
}

int findDist(Node* root, int a, int b) {
    Node* lca = getLCA(root, a, b);
    return distFromRoot(root, a) + distFromRoot(root, b) - 2 * distFromRoot(root, lca->data);
}