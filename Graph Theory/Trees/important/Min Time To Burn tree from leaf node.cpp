#include "../Tree.h"

/**
 * Very complex problem
 * https://www.geeksforgeeks.org/minimum-time-to-burn-a-tree-starting-from-a-leaf-node/?ref=leftbar-rightbar
 * https://www.youtube.com/watch?v=wwv7xcNUhRA (printing order in which we burn)
 */

//Additional information stored at each node
struct Info {
    int height;    //height of the subtree at this node
    int dist;      //dist from this node to the target leaf (if it exists in one of its subtrees. else it is -1)
    bool isFound;  //if we found target leaf in one of its subtrees [dont need this actually. if dist==-1 then target not found]

    Info(int height, int dist, bool isFound) : height(height), dist(dist), isFound(isFound) {}
};

Info minTime(Node *root, int leaf, int &result) {
    if (!root) {
        return Info(0, -1, false);
    }
    //found target leaf
    if (root->data == leaf) {
        return Info(1, 0, true);
    }
    //any other leaf node
    if (root->left == NULL && root->right == NULL) {
        return Info(1, -1, false);
    }

    Info left = minTime(root->left, leaf, result);
    Info right = minTime(root->right, leaf, result);

    int dist = -1;
    if (left.isFound) {
        //if present in left subtree
        result = max(result, 1 + left.dist + right.height);
        dist = 1 + left.dist;
    } else if (right.isFound) {
        //if present in right subtree
        result = max(result, 1 + right.dist + left.height);
        dist = 1 + right.dist;
    }
    return Info(max(left.height, right.height) + 1, dist, left.isFound || right.isFound);
}

int solve(Node *root, int leaf) {
    int result = 0;
    minTime(root, leaf, result);
    return result;
}

int main() {
    // Node *root = new Node(1);
    // root->left = new Node(2);
    // root->right = new Node(3);
    // root->left->left = new Node(4);
    // root->left->right = new Node(5);
    // root->right->left = new Node(6);
    // root->left->left->left = new Node(8);
    // root->left->right->left = new Node(9);
    // root->left->right->right = new Node(10);
    // root->left->right->left->left = new Node(11);
    // int target = 11;
    // cout << solve(root, target)<<endl; //6

    // Node *root = new Node(1);
    // root->left = new Node(2);
    // root->left->left = new Node(4);
    // root->left->right = new Node(5);
    // root->left->right->left = new Node(7);
    // root->left->right->right = new Node(8);
    // root->right = new Node(3);
    // root->right->right = new Node(6);
    // root->right->right->right = new Node(9);
    // root->right->right->right->right = new Node(10);
    // cout << solve(root, 8) << endl;   //7
    // cout << solve(root, 7) << endl;   //7
    // cout << solve(root, 4) << endl;   //6
    // cout << solve(root, 10) << endl;  //7

    Node *root = new Node(1);
    root->left = new Node(2);
    root->right = new Node(3);
    cout << solve(root, 2) << endl;  //2
    cout << solve(root, 3) << endl;  //2
    return 0;
}