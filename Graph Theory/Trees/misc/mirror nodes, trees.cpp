#include "../Tree.h"

/*
https://www.geeksforgeeks.org/find-mirror-given-node-binary-tree/
find mirror node for the given node
    If we go left to search for the given node then we need to go right for the mirror node and vice-versa
    So while searching for the given node, also store mirror of the current node
*/

Node* findMirrorRecursive(Node* current, Node* currentMirror, int target) {
    if (!current) return NULL;

    if (current->data == target) return currentMirror;

    Node* left = findMirrorRecursive(current->left, currentMirror->right, target);
    Node* right = findMirrorRecursive(current->right, currentMirror->left, target);

    if (left != NULL) return left;
    return right;
}

Node* findMirror(Node* root, int target) {
    return findMirrorRecursive(root, root, target);
}

/*
https://practice.geeksforgeeks.org/problems/mirror-tree/1
convert tree into its mirror tree
*/
void mirror(Node* node) {
    if (!node) return;

    mirror(node->left);
    mirror(node->right);
    //node->left has mirror of left subtree
    //node->right has mirror of right subtree
    Node* temp = node->left;
    node->left = node->right;
    node->right = temp;
}