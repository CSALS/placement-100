#include "Tree.h"

/**
 * https://leetcode.com/problems/find-mode-in-binary-search-tree/
 * 1st approach will be to use hashing. O(N) Time | O(N) Space
 * 2nd approach will be to maintain previous node and compare with current node while doing inorder
 * traversal since in inorder it will be sorted manner (same elements will occur consecutively)
*/

//Visit the current node. Update the mode
void visitNode(Node*& current, Node*& previous, int& currentCount, int& maximumCount, vector<int>& modes) {
    if (previous == NULL) {
        previous = current;
        currentCount = 1;
        maximumCount = 1;
        modes.push_back(current->val);
    } else if (previous->val == current->val) {
        previous = current;
        currentCount++;
        if (currentCount == maximumCount) {
            modes.push_back(current->val);
        } else if (currentCount > maximumCount) {
            cout << "e\n";
            maximumCount = currentCount;
            modes.clear();
            modes.push_back(current->val);
        }
    } else {
        previous = current;
        currentCount = 1;
        if (currentCount == maximumCount) {
            modes.push_back(current->val);
        }
    }
    current = current->right;
}
Node* getPredecessor(Node* current) {
    Node* predecessor = current->left;
    while (predecessor->right != NULL && predecessor->right != current) {
        predecessor = predecessor->right;
    }
    return predecessor;
}
vector<int> findMode(Node* root) {
    Node *previous = NULL, *current = root;
    int currentCount = 0, maximumCount = INT_MIN;
    vector<int> modes;

    //Morris inorder traversal = O(N)T | O(1)S
    while (current != NULL) {
        if (current->left == NULL) {
            visitNode(current, previous, currentCount, maximumCount, modes);
        } else {
            Node* pred = getPredecessor(current);
            if (pred->right == NULL) {
                pred->right = current;
                current = current->left;
            } else {
                pred->right = NULL;
                visitNode(current, previous, currentCount, maximumCount, modes);
            }
        }
    }
    return modes;
}