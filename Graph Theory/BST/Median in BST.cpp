#include "Tree.h"

Node* getInorderPredecessor(Node* current) {
    Node* predecessor = current->left;
    while (predecessor->right != NULL && predecessor->right != current) {
        predecessor = predecessor->right;
    }
    return predecessor;
}

//O(K) Time | O(1) Space
int getKthSmallestInBST(Node* root, int K) {
    Node* current = root;
    int count = 0;
    int key = -1;
    while (current != NULL) {
        if (current->left == NULL) {
            ++count;
            if (count == K) {
                key = current->data;
                // break; don't break when you found the value since you are modifying tree structure. let entire traversal complete
            }
            // cout<<count<<" "<<current->data<<endl;
            current = current->right;
        } else {
            Node* predecessor = getInorderPredecessor(current);
            if (predecessor->right == NULL) {
                predecessor->right = current;
                current = current->left;
            } else if (predecessor->right == current) {
                predecessor->right = NULL;
                ++count;
                if (count == K) {
                    key = current->data;
                }
                current = current->right;
            }
        }
    }
    return key;
}

void countBSTNodes(Node* root, int& count) {
    if (root == NULL) return;
    countBSTNodes(root->left, count);
    ++count;
    countBSTNodes(root->right, count);
}

//Median uses kth smallest element in BST method
float findMedian(Node* root) {
    int count = 0;
    countBSTNodes(root, count);
    float middle1 = 1.0 * getKthSmallestInBST(root, (count + 1) / 2);
    if (count & 1) {
        return middle1;
    } else {
        float middle2 = 1.0 * getKthSmallestInBST(root, (count + 2) / 2);
        return (middle1 + middle2) / 2;
    }
}