# Tricks
- **think of using {min, max} of subtrees**

- Two recursions = Top-down and bottom-up
Use bottom-up for optimization over top-down approach

# Important
1. delete node from bst
2. bst from preorder (without using inorder)
3. bst from level order (Only one possible if BST. If BT then many)
4. common nodes in 2 bst

# Problems
* Find Two Node From Same BST which add up to a sum K = https://tinyurl.com/yazkgbg5
    1. Inorder Traversal & Reduces to sorted array and target sum. Two pointer. one from 0 and one from n-1.
        Inorder can be done using Morris-Traversal (constant space)
        O(N) Time & Space
    2. Inorder Traversal + Hashing
        O(N) Time & Space
    3. Using Fwd & Bwd BST Iterator as two pointer technique
* Find Two Nodes From 2 BSTs which add up to a sum K =
    1. Inorder Traversal & Reduces to two sorted arrays and target sum. Two pointer. one from 0 and one from n-1.
        Inorder can be done using Morris-Traversal (constant space)
        O(N + M) Time & Space

