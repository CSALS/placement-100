#include <bits/stdc++.h>
using namespace std;

//https://www.geeksforgeeks.org/mobile-numeric-keypad-problem/

unordered_map<int, vector<int>> child;
int dp[50][50];

int top_down(int N, int start) {
    if (N == 0) return 0;
    if (N == 1) return 1;

    if (dp[N][start] != -1) return dp[N][start];

    dp[N][start] = 0;
    for (int next : child[start]) {
        dp[N][start] += top_down(N - 1, next);
    }
    return dp[N][start];
}

int solution() {
    int n;
    cin >> n;
    memset(dp, -1, sizeof(dp));
    child[0] = {0, 8};
    child[1] = {1, 2, 4};
    child[2] = {1, 2, 3, 5};
    child[3] = {2, 3, 6};
    child[4] = {1, 4, 5, 7};
    child[5] = {2, 4, 5, 6, 8};
    child[6] = {3, 5, 6, 9};
    child[7] = {4, 7, 8};
    child[8] = {0, 5, 7, 8, 9};
    child[9] = {6, 8, 9};
    //
    int ways = 0;
    for (int i = 0; i <= 9; ++i) {
        dp[n][i] = top_down(n, i);
        ways += dp[n][i];
    }
    return ways;
}