/*

https://www.geeksforgeeks.org/minimum-increment-or-decrement-operations-required-to-make-the-array-sorted/

Given an array arr[] of N integers, the task is to sort the array in non-decreasing order by performing the minimum number of operations. 
In a single operation, an element of the array can either be incremented or decremented by 1. 
Print the minimum number of operations required.

    IMP Observation => Any number shouldn't go below min. number and also shouldn't go above max. number

    dp[0.....N-1][minEle.....maxEle]

    dp[i][j] = making [0....i] sorted and last element will be j

    //Base Case
    for(int j = minEle; j <= maxEle; ++j)
        dp[0][j] = abs(j - arr[0]) //making first element as j

    //Recurrence
    for(int i = 1; i < n; ++i) {
        for(int j = minEle; j <= maxEle; ++j) {
            dp[i][j] = INF
            for(int k = 1; k <= j; ++k) {
                dp[i][j] = min(dp[i][j], dp[i - 1][k] + abs(arr[i] - j))
            }
        }
    }

    //Recurrence-Optimized
    for(int i = 1; i < n; ++i) {
        //maintain minimum of dp[i - 1][j] till now and use that for dp[i][j]
        int min = INF
        for(int j = minEle; j <= maxEle; ++j) {
            min = min(min, dp[i - 1][j])
            dp[i][j] = min + abs(arr[i] - j) //min stores minimum of dp[i-1][k] where k is 1...j
        }
    }

    //Answer
    int ans = INF
    for(int j = minEle; j <= maxEle; ++j) 
        ans = min(ans, dp[N - 1][j])
*/