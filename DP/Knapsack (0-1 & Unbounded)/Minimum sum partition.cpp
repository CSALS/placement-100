#include <bits/stdc++.h>
using namespace std;
#define deb(x) cout << #x << " " << x << endl;

/**
 * https://practice.geeksforgeeks.org/problems/minimum-sum-partition/0
 * Given an array, the task is to divide it into two sets S1 and S2 such that the absolute difference between their sums is minimum
 */
int minSubDiff(int n, vector<int> &arr, int totalSum) {
    //subset sum
    bool dp[n + 1][totalSum + 1];
    dp[0][0] = true;
    for (int i = 1; i <= totalSum; ++i)
        dp[0][i] = false;
    for (int i = 1; i <= n; ++i)
        dp[i][0] = true;

    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= totalSum; ++j) {
            dp[i][j] = (arr[i - 1] <= j) ? dp[i - 1][j] || dp[i - 1][j - arr[i - 1]] : dp[i - 1][j];
        }
    }

    /*
        s1 + s2 = totalSum
        |s1 - s2| = |totalSum - 2*s1|
        s1 can be [0, totalSum/2]. s2 is [totalSum/2, totalSum]
        min |totalSum - 2*s1| => max possible s1
    */

    for (int s1 = totalSum / 2; s1 >= 0; --s1) {
        if (dp[n][s1] == true)
            return totalSum - 2 * s1;
    }
    return -1;
}

int solution() {
    int n;
    cin >> n;
    vector<int> arr(n);
    int totalSum = 0;
    for (int &x : arr) {
        cin >> x;
        totalSum += x;
    }
    return minSubDiff(n, arr, totalSum);
}
int32_t main() {
    int t;
    cin >> t;
    while (t--) {
        cout << solution() << endl;
    }
    return 0;
}