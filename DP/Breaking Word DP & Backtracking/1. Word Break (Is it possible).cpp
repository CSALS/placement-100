#include <bits/stdc++.h>
using namespace std;

/**
 * https://leetcode.com/problems/word-break/
 * Given a string, break into segments such that each segment belongs to the given dictionary. 
 * Need to check if it is possible or not?
*/

class IsItPossibe {
    unordered_map<string, bool> exists;
    string str;
    vector<int> dp;

   public:
    bool wordBreak(string s, vector<string>& wordDict) {
        exists.clear();
        for (string word : wordDict) {
            exists[word] = true;
        }
        dp.resize(s.length(), -1);  //dp[i] = is it possible to break the str[i....N-1]
        str = s;
        //
        return wordBreakRecursive(0);
    }

    bool wordBreakRecursive(int index) {
        int n = str.length();

        if (index >= n) {
            return true;
        }

        if (dp[index] != -1) {
            return dp[index];
        }

        string prefix = "";
        for (int i = index; i < n; ++i) {
            prefix += str[i];
            if (exists[prefix]) {
                //[0....i] exists in the dictionary
                //check if [i+1...n-1] can be segmented. if yes then we can segment [index....n-1]
                if (wordBreakRecursive(i + 1)) {
                    return dp[index] = 1;
                }
            }
        }
        return dp[index] = 0;
    }
};