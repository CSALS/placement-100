#include <bits/stdc++.h>
using namespace std;

/*
1. Min. number of insertions and deletions to convert A to B
    heap => pea (1 insert + 2 delete)
    Answer = Delete (len(A) - LCS) + Insert (len(B) - LCS)
2. Edit Distance Problem = Min. number of insertions, deletions, replacing to convert A to B
*/
const int MAX_N = 1020;
class EditDistance {
    int dp[MAX_N][MAX_N];

   public:
    //can perform operations only on A and should convert it to B
    int top_down(string &A, string &B, int n1, int n2) {
        if (n1 == 0) {
            return n2;  //n2 insertions required
        }
        if (n2 == 0) {
            return n1;  //n1 deletions required
        }

        if (dp[n1][n2] != -1) {
            return dp[n1][n2];
        }

        if (A[n1 - 1] == B[n2 - 1]) {
            dp[n1][n2] = top_down(A, B, n1 - 1, n2 - 1);
        } else {
            //delete A[n1 - 1]
            dp[n1][n2] = top_down(A, B, n1 - 1, n2) + 1;
            //insert B[n2 - 1] at (n1 - 1)th position in A
            dp[n1][n2] = min(dp[n1][n2], top_down(A, B, n1, n2 - 1) + 1);
            //replace A[n1 - 1] with B[n2 - 1]
            dp[n1][n2] = min(dp[n1][n2], top_down(A, B, n1 - 1, n2 - 1) + 1);
        }
        return dp[n1][n2];
    }
    int minDistance(string A, string B) {
        memset(dp, -1, sizeof(dp));
        return top_down(A, B, A.length(), B.length());
    }
};
