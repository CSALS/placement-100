#include <bits/stdc++.h>
using namespace std;

/**
 * Longest Increasing subsequence
 * Three Approaches.
 * 1. DP Solution. LIS = LCS between original array and its sorted array O(N^2 + NlogN)
 * 2. DP Solution. Using lis[i] = longest increasing subsequence ending at i. O(N^2)
 * 3. Binary Search = O(NlogN)
 */

/**
 * Use another array to store increasing subsequence
 * If current element > last element in the other array => Then append it
 * If current element < last element then replace it at the first position where it is greater 
 * The position = (lower_bound in case of strictly increasing subsequence, upper_bound in case of non-decreasing subsequence)
 * lower_bound(x) [first element >= x]
 * upper_bound(x) [first elemnt > x]
 */
int lis_binarysearch(vector<int> &arr) {
    int n = arr.size();
    if (n == 0) return 0;
    vector<int> tail(n);  //tail always stores elements in increasing order
    tail[0] = arr[0];
    int length = 1;  //length of tail filled
    for (int i = 1; i < n; ++i) {
        int curr_element = arr[i];
        if (curr_element > tail[length - 1]) {
            tail[length] = curr_element;
            length++;
        } else {
            int index = lower_bound(tail.begin(), tail.begin() + length, curr_element) - tail.begin();
            tail[index] = curr_element;
        }
    }
    return length;
}

/**
 * Maintaining subsequences using parent[] array.
 * Array = [3, 1, 5, 2, 6, 4, 9]
 * How the above algo works?
 * 3
 * 1 <-- 5
 * 1 <-- 2 <-- 6
 * 1 <-- 2 <-- 4 <-- 9
 */

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * Printing all increasing subsequences
 * https://www.geeksforgeeks.org/print-all-increasing-subsequence-of-a-list/
 */

void printArray(vector<int> &arr) {
    for (int num : arr) cout << num << " ";
    cout << endl;
}

vector<vector<int>> result;

void printISRecursive(vector<int> &arr, vector<int> subsequence, int ind) {
    if (ind >= arr.size()) {
        if (subsequence.size() > 1)
            printArray(subsequence);
        return;
    }

    int prev_ele = INT_MIN;  //if subsequence is empty then we can include or exclude current element
    if (!subsequence.empty())
        prev_ele = subsequence[subsequence.size() - 1];
    int curr_ele = arr[ind];

    if (prev_ele < curr_ele) {
        //Two choices. Include it or don't
        printISRecursive(arr, subsequence, ind + 1);
        subsequence.push_back(curr_ele);
        printISRecursive(arr, subsequence, ind + 1);
    } else {
        //Only on choice. Cant include
        printISRecursive(arr, subsequence, ind + 1);
    }
}

void printIS(vector<int> &arr) {
    printISRecursive(arr, {}, 0);
}

/**
 * Counting all strictly increasing subsequences
 * 1. Using normal DP LIS = works for any range of elements [O(N^2) Time | O(N) Space]
 * 2. If given elements are in range of [0, 9] then can reduce time [O(N) Time | O(1) Space]
 */

//count all increasing subsequences in any array
int countIS(vector<int> &arr) {
    int n = arr.size();
    if (n == 0) return 0;
    vector<int> dp(n, 1);  //dp[i] = number of IS ending at i

    for (int i = 1; i < n; ++i) {
        for (int j = 0; j < i; ++j) {
            if (arr[j] < arr[i])
                dp[i] += dp[j];
        }
    }

    int count = 0;
    for (int ct : dp)
        count += ct;
    return count;
}
//count all increasing subsequences in array containing [0, 9] elements
//since all numbers are between [0, 9] we can maintain count of the elements while iterating
//you don't need to have an inner loop to go back but rather can check only the count array
int countIS(vector<int> &arr) {
    int n = arr.size();
    if (n == 0) return 0;

    int count[10] = {0};  //count[i] = number of IS ending at value 'i'
    for (int i = 0; i < n; ++i) {
        //count[d] stores current count digits smaller than d and to the left of it in the array
        //so after entire iteration it stores all IS ending at value 'i'
        int ele = arr[i];
        count[ele] += 1;  // For [ele] subsequence
        for (int j = 0; j < ele; ++j) {
            count[ele] += count[j];
        }
    }

    int final_count = 0;
    for (int i = 0; i < 10; ++i)
        final_count += count[i];
    return final_count;
}